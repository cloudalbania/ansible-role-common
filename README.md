Ansible Role Common
=========

Common role for base server config

Requirements
------------

No requirments

Role Variables
--------------

variables are set in group_vars

Dependencies
------------

    - geerlingguy.repo-epel
    - besmirzanaj.ansible_resolv_conf

Example Playbook
----------------

Including an example of how to use the role:

    - hosts: servers
      roles:
         - { role: common }

License
-------

CC-BY-4.0

Author Information
------------------

Besmir Zanaj besmirzanaj@gmail.com 
